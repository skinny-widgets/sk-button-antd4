
import { AntdSkButton }  from '../../sk-button-antd/src/antd-sk-button.js';

export class Antd4SkButton extends AntdSkButton {

    get prefix() {
        return 'antd4';
    }

}
